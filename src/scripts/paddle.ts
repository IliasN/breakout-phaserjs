// https://photonstorm.github.io/phaser3-docs/Phaser.GameObjects.GameObject.html
export default class Paddle extends Phaser.GameObjects.GameObject {

  image: Phaser.Physics.Arcade.Image;
  
  constructor(scene: Phaser.Scene) {
    super(scene, "paddle");
    
    this.create();
  }

  create() {
    const canvas = this.scene.game.canvas;
    this.image = this.scene.physics.add.image(canvas.width / 2, canvas.height - 50, 'assets', 'paddle1').setImmovable();

    this.scene.input.on('pointermove', function (pointer) {

      //  Limite le mouvement du plateau aux extrémités gauches et droites
      this.image.x = Phaser.Math.Clamp(pointer.x, 52, canvas.width - 52);

      this.emit("paddleMovedEvent", this.image);

    }, this);
  }

};
